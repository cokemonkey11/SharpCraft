﻿using System.Collections.Generic;

namespace SharpCraft
{
    public class PluginInfo
    {
        public PluginInfo(string name, string description, string version, params string[] authors)
        {
            this.Name = name;
            this.Description = description;
            this.Version = version;
            this.Authors = new List<string>(authors);
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Version { get; set; }

        public List<string> Authors { get; set; }
    }
}
