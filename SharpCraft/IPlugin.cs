﻿using System.ComponentModel.Composition;

namespace SharpCraft
{
    public delegate void PluginReadyEventHandler(object sender);

    [InheritedExport(typeof(IPlugin))]
    public interface IPlugin
    {
        event PluginReadyEventHandler Ready;

        PluginInfo Info { get; }

        bool IsReady { get; }

        void Initialize();
    }
}
