﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using EasyHook;
using Microsoft.Win32;
using MindWorX.Unmanaged.Windows;

namespace SharpCraft
{
    public unsafe class ProfileLauncher
    {
        public bool Launch(Profile profile)
        {
            if (String.IsNullOrEmpty(profile.FileName) || !File.Exists(profile.FileName))
            {
                if (MessageBox.Show("Profile not configured." + Environment.NewLine + Environment.NewLine + "Configure profile now?", "Incomplete Profile", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    return false;

                var dialog = new OpenFileDialog
                {
                    Filter = "All Files|*.*|Starcraft II - Editor|sc2editor.exe|Starcraft II - Game|sc2.exe|Warcraft III - Editor|worldedit.exe|Warcraft III - Game|war3.exe",
                };

                if (profile.Type == ProfileType.BlizzardStarcraftIIEditor)
                    dialog.FilterIndex = 2;
                else if (profile.Type == ProfileType.BlizzardStarcraftIIGame)
                    dialog.FilterIndex = 3;
                else if (profile.Type == ProfileType.BlizzardWarcraftIIIEditor)
                    dialog.FilterIndex = 4;
                else if (profile.Type == ProfileType.BlizzardWarcraftIIIGame)
                    dialog.FilterIndex = 5;

                if (dialog.ShowDialog() == true)
                {
                    profile.FileName = dialog.FileName;
                    profile.WorkingDirectory = Path.GetDirectoryName(profile.FileName);
                    profile.Save();
                }
                else
                {
                    return false;
                }
            }

            return profile.EnableSharpCraft ? this.LaunchSharpCraft(profile) : this.LaunchVanilla(profile);
        }

        private bool LaunchVanilla(Profile profile)
        {
            var processStartInfo = new ProcessStartInfo
            {
                FileName = profile.FileName,
                Arguments = profile.Arguments,
                WorkingDirectory = profile.WorkingDirectory
            };
            return Process.Start(processStartInfo) != null;
        }

        private bool LaunchSharpCraft(Profile profile)
        {
            var startupInfo = new STARTUPINFO();
            PROCESS_INFORMATION processInformation;
            if (!Kernel32.CreateProcessA(profile.FileName, "\"" + profile.FileName + "\" " + profile.Arguments, null, null, false, ProcessCreationFlags.CREATE_SUSPENDED, null, profile.WorkingDirectory, ref startupInfo, out processInformation))
                return false;

            RemoteHooking.Inject((int)processInformation.dwProcessId, "SharpCraft.dll", "SharpCraft.dll", processInformation.dwThreadId, profile);
            return true;
        }
    }
}
