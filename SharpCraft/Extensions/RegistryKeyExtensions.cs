﻿using Microsoft.Win32;

namespace SharpCraft
{
    public static class RegistryKeyExtensions
    {
        public static RegistryKey OpenOrCreateSubKey(this RegistryKey root, string name) => root.OpenSubKey(name) ?? root.CreateSubKey(name);

        public static RegistryKey OpenOrCreateSubKey(this RegistryKey root, string name, bool writable) => root.OpenSubKey(name, writable) ?? root.CreateSubKey(name, writable);
    }
}
