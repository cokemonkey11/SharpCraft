﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Serilog;
using Serilog.Events;

namespace SharpCraft
{
    public class SharpCraftEnvironment
    {
        public SharpCraftEnvironment(string name, string root)
        {
            this.Name = name;
            this.Root = root;

            if (!Directory.Exists(this.Root))
                Directory.CreateDirectory(this.Root);
            if (!Directory.Exists(this.LogsDirectory))
                Directory.CreateDirectory(this.LogsDirectory);
            if (!Directory.Exists(this.ProfilesDirectory))
                Directory.CreateDirectory(this.ProfilesDirectory);
            if (!Directory.Exists(this.PluginsDirectory))
                Directory.CreateDirectory(this.PluginsDirectory);

            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("Version", FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion)
                .Enrich.WithProperty("Environment", this.Name)
                .WriteTo.LiterateConsole()
#if DEBUG
                .MinimumLevel.Debug()
                .WriteTo.RollingFile(Path.Combine(this.LogsDirectory, @"log-{Date}.debug.txt"), LogEventLevel.Debug,
                "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Environment}][{Version}][{Level}] {Message}{NewLine}{Exception}", shared: true)
#endif
                .WriteTo.RollingFile(Path.Combine(this.LogsDirectory, @"log-{Date}.txt"), LogEventLevel.Information,
                "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Environment}][{Version}][{Level}] {Message}{NewLine}{Exception}", shared: true)
                .CreateLogger();
        }

        public string Name { get; }

        public string Root { get; }

        public IList<Profile> Profiles { get; } = new List<Profile>();

        public string LogsDirectory => Path.Combine(this.Root, "logs");

        public string ProfilesDirectory => Path.Combine(this.Root, "profiles");

        public string PluginsDirectory => Path.Combine(this.Root, "plugins");

        public void LoadProfiles()
        {
            if (this.Profiles.Any())
                this.Profiles.Clear();

            foreach (var profileDirectory in Directory.EnumerateDirectories(this.ProfilesDirectory, "*.*", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    var profile = Profile.Load(profileDirectory);
                    Log.Logger?.Information("Loaded {@profile}!", profile);
                    this.Profiles.Add(profile);
                }
                catch (Exception e)
                {
                    Log.Logger?.Error(e, "Unable to load profile at {profileDirectory}.", profileDirectory);
                }
            }
        }
    }
}
