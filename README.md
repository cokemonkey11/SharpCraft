[![sharpcraft MyGet Build Status](https://www.myget.org/BuildSource/Badge/sharpcraft?identifier=fef7e655-47f7-4f4a-be45-de8bdc3ddd17)](https://www.myget.org/)

SharpCraft
==========
SharpCraft is a system for modding Warcraft III with plugins. It is based on MEF for injecting the additional dependencies.

SharpCraft is simply the core, and any additional functionality is handled through plugins and different profiles.